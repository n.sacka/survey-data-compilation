# Survey Data Compilation

The R file CompiledResponses.R will output all the responses compiled into one table, translated into English, in an excel file.

**Please Note:** The data and other supporting files needed to run CompiledResponses.R has not been included and cannot be made available.

# Overview

This script turns data from excel files into one usable table and exports it to excel.

The nature of the data itself is tricky to handle. As it is survey data, the column names are often quite long and many contain the same starting sentence. In addition, the column names may contain hyperlinks or descriptions. For this reason, the package __surveydata__ was used to make the data easier to work with. __surveydata__ enables you to call the columns something shorter, like "Q1," while preserving the original column name elsewhere.

The survey data is bilingual, meaning that each question exists in English and French. This means there are twice the number of columns in the dataset as there are questions in the survey. To deal with this, the corresponding columns have been merged into one column, and French answers have been translated into one English (or numbers where possible) for ease of analysis. (It could be translated into French as well, you'd reference the same  excel translation file, and change the code slightly.)

# Technologies

Main R packages:

- __readxl__ and __openxlsx__ 
    - Reading, creating, and writing to excel files.
- __dplyr__ 
    - Data frame manipulation.
- __surveydata__
    - Simplifies survey data set for easier handling.
    
# A Note About Installing and Running

The data needed for this project has __not__ been included in this repo. It cannot be made available. 

For this reason, this is a showcase project and is not intended to be run.
